## Selenium UI. Page Object, Allure, Selenoid
Что было сделано:

* 2 негативных теста на авторизацию

* Тест на создание сегмента

* Тест на удаление сегмента

* Тест на создание кампании

* Весь код реализован на паттерне Page Object

* Возможность просмотреть отчеты в Allure

* Возможность запустить UI тесты в Selenoid 

